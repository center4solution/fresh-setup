import * as ENV from "dotenv";
ENV.config();


const SHORT_CODE = "";

export const CONFIG = {
   APP_NAME: process.env.APP_NAME || '',
   APP_ENV: process.env.APP_ENV || '',
   PORT: process.env.PORT || '',

   SHORT_CODE: SHORT_CODE,

   KEYS: {
      SECRET: '',
   },

   REDIS_CONN: process.env.REDIS_CONN || '',

   DB: {
      USER: process.env.DB_USER || '',
      PASSWORD: process.env.DB_PASSWORD || '',
      DB_NAME: process.env.DB_NAME || '',
      HOST: process.env.DB_HOST || '',
      HOST_READ: process.env.HOST_READ || '',
   }
   
};
