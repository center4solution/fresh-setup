export enum BooleanEnum {
  false = "0",
  true = "1",
}

export enum StatusEnum {
  ACTIVE = "active",
  INACTIVE = "inactive",
  DELETED = "deleted",
  DISABLED = "disabled",
}
