import { Sequelize } from "sequelize";

export interface DBInterface {
  conn_write: Sequelize,
  conn_read: Sequelize

}

