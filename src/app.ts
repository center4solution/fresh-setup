import express, { Request, Response, NextFunction } from "express";
import helmet from "helmet";
import cors from "cors";
import Ddos from "ddos";

import logs from "./middlewares/logs.middleware";
import trimAll from "./middlewares/trim.middleware";
import maintenance from "./middlewares/maintenance.middleware";

class App {
  public app: express.Application;
  public port: number;
  public server: any;
  public io: any;


  constructor(controllers: any, port: any) {
    this.app = express();
    this.port = port;

    this.initializeMiddlewares();
    this.apiLogs();


    this.initializeControllers(controllers);
    this.swaggerInitialize();

    this.checkHealth();
  }

  private initializeMiddlewares() {
    // this.app.use(ddos.express);

    const corsOptions = {
      credentials: true,
      // origin: true,
      origin: '*'
    };
    this.app.use(cors(corsOptions));

    this.app.use(express.json({ limit: "1mb" }));
    this.app.use(express.urlencoded({
      limit: "1mb",
      extended: true,
      parameterLimit: 50000
    }));

    // this.app.use(
    //    `/api/static/`,
    //    express.static(path.join(__dirname, "public"))
    // );

    this.app.use(helmet());
    this.app.use(helmet.contentSecurityPolicy({
      useDefaults: true,
      directives: {
        "frame-ancestors": "none",
      },
    }));
    this.app.use(helmet.dnsPrefetchControl());
    // this.app.use(helmet.expectCt());
    this.app.use(helmet.frameguard({
      action: "deny",
    }));
    this.app.use(helmet.hidePoweredBy());
    this.app.use(helmet.hsts());
    this.app.use(helmet.ieNoOpen());
    this.app.use(helmet.noSniff());
    this.app.use(helmet.permittedCrossDomainPolicies());
    this.app.use(helmet.referrerPolicy());
    this.app.use(helmet.xssFilter());


    /** Request Decrypation*/
    this.app.use(logs.RequestData);
    this.app.use(trimAll.trim);

  }

  private initializeControllers(controllers: any) {
    controllers.forEach((controller: any) => {

      // this.app.use(
      //   '/api',
      //   maintenance.MaintenanceMode,
      //   controller?.router
      // );

      controller.routes.forEach((route: any) => {
        this.app.use(
          '/api/user',
          maintenance.MaintenanceMode,
          route.router
        );
      });

      /** api routes and controller for admin dashboard */
      if (controller.type === "admin") {
        controller.routes.forEach((route: any) => {
          this.app.use(
            '/api/admin',
            route.router
          );
        });
      }

    });
  }

  public listen() {
    this.app.listen(this.port, () => {
      console.error(`App listening on the port ${this.port}`);
    });
  }

  private apiLogs() {
    this.app.use(async (
      req: Request,
      res: Response,
      next: NextFunction
    ) => {
      let method: string = req.method ? req.method : '';
      let url: string = req.url ? req.url : '';
      console.error(`URL===>>`, method, `----->`, url);
      next();
    });
  }

  public swaggerInitialize() {

  }

  public checkHealth() {
    this.app.use("/", (req: Request, res: Response, next: NextFunction) => {
      const healthcheck: any = {
        uptime: process.uptime(),
        message: 'OK',
        timestamp: Date.now()
      };
      try {
        res.status(200).send(healthcheck);
      } catch (error) {
        healthcheck.message = error;
        res.status(503).send();
      }
    })
  }
  
}
export default App;
