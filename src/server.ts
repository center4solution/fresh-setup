import App from "./app";
import { CONFIG } from "./config";
import { AdminModuleRoutes, UserModuleRoutes } from "./modules";

const app = new App([
   {
      type: "user",
      routes: UserModuleRoutes,
   }, {
      type: "admin",
      routes: AdminModuleRoutes,
   }
],
   CONFIG.PORT
);
app.listen();
