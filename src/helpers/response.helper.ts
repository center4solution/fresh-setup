import { RESPONSES } from "../constant/response";

class ResponseHelper {
   public async success(response: any, responseData: any = {}) {
      if (response.headersSent !== true) {
         return response.status(RESPONSES.SUCCESS).send(responseData);
      }
   }

   public error(response: any, responseData: any = {}) {
      if (response.headersSent !== true) {
         return response.status(RESPONSES.BADREQUEST).send(responseData);
      }
   }
   public unauthorized(response: any, responseData: any = {}) {
      if (response.headersSent !== true) {
         return response.status(RESPONSES.UNAUTHORIZED).send(responseData);
      }
   }
   public serviceUnavilable(response: any, responseData: any = {}) {
      if (response.headersSent !== true) {
         return response.status(RESPONSES.SERVICEUNAVILABLE).send(responseData);
      }
   }

}
export let ReturnResponse = new ResponseHelper();
