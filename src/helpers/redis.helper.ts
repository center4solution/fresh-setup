import { Redis } from "../connections";

class RedisHelper {
   SetHashTable = async (
      key: string,
      data: any,
      value: any,
      expires: any = 0,
   ) => {
      try {
         return await Redis.setHashTable(
            key.toLowerCase(),
            data,
            value,
            expires
         );
      } catch (error: any) {
         console.error(`Redis_Helper SetHashTable error >>>`, error);
         return {};
      }
   }

   SetHashTableWithExpiry = async (
      key: string,
      data: any,
      value: any,
      expires: any,
   ) => {
      try {
         console.log(`SetHashTableWithExpiry expires >>> `, expires);
         return await Redis.setHashTable(
            key.toLowerCase(),
            data,
            value,
            expires,
         );
      } catch (error: any) {
         console.error(`Redis_Helper SetHashTableWithExpiry error >>>`, error);
         return false;
      }
   }

   GetHashTableAllData = async (
      key: any
   ) => {
      try {
         const data: any = await Redis.getHashTableAllData(key.toLowerCase());
         return data;
      } catch (error: any) {
         console.error(`Redis_Helper GetHashTableAllData error >>>`, error);
         return {};
      }
   }

   GetKeyData = async (
      key: string
   ) => {
      try {         
         return await Redis.getString(key.toLowerCase());
      } catch (error: any) {
         console.error(`Redis_Helper GetKeyData ( ${key} ) >>`, error);
         return false;
      }
   }

   UpdateKeyData = async (
      key: string,
      data: any,
      expires = 0
   ) => {
      try {
         return await Redis.setString(
            key.toLowerCase(),
            JSON.stringify(data ? data : {}),
            expires
         );
      } catch (error: any) {
         console.error(`Redis_Helper UpdateKeyData >>`, error);
         return false;
      }
   }

   DeleteKey = async(
      key: string
   ) => {
      try {         
         return await Redis.deleteString(key.toLowerCase());
      } catch (error: any) {
         console.error(`Redis_Helper DeleteKey >>`, error);
         return false;
      }
   }
}
export let Redis_Helper = new RedisHelper();
