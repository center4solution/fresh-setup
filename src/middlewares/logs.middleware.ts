import { Request, Response, NextFunction } from 'express';
import fs from "fs";
import path from "path";
import NodeRSA from "node-rsa";

import {
   RES_MSG
} from '../constant';

import {
   ReturnResponse
} from '../helpers';

class logClass {
   public async RequestData(
      req: Request,
      res: Response,
      next: NextFunction
   ) {
      const lanCode: any = req.language;
      
      try {
         if (
            req.method == "POST" &&
            typeof req.body != "undefined" &&
            typeof req.body.datastring != "undefined" &&
            Array.isArray(req.body.datastring) == false
         ) {
            let privateKeyFile = fs.readFileSync(`${path.resolve(__dirname, '../config/_keys/enc_keys/rsa_2048_private_key.pem')}`, { encoding: "utf8" });
            let privateKey: any = Buffer.from(privateKeyFile);
            let RSAKey = new NodeRSA(privateKey)
            RSAKey.setOptions({ encryptionScheme: 'pkcs1' });
            let decryptedData = RSAKey.decrypt(req?.body?.datastring, 'utf8');
            req.body = JSON.parse(decryptedData);
            next();
         } else {
            next();
         }
      } catch (error: any) {
         console.error({ status: "error", msg: error });

         const resMsg = {
            status: false,
            message: RES_MSG.ERROR
         };
         return ReturnResponse.error(res, resMsg);
      }
   }
}
const logs = new logClass();
export default logs;
