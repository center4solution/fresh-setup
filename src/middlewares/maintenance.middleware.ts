import { NextFunction, Request, Response } from 'express';


class maintenanceClass {

	MaintenanceMode = async (
		req: Request,
		res: Response,
		next: NextFunction
	) => {
		// const getMaintenanceMode: string = await Utility_Helper.GetSetting(`enable_server_maintenance_mode`);
		// if (!getMaintenanceMode || getMaintenanceMode == 'yes') {
		// 	const resMsg = {
		// 		error: true,
		// 		status: false,
		// 		message: RES_MSG.MAINTENANCE_MODE,
		// 		server_maintenance_mode_enable: BooleanEnum.true,
		// 	};
		// 	return ReturnResponse.serviceUnavilable(res, resMsg);
		// }
		next();
	}
}
const maintenance = new maintenanceClass();
export default maintenance;
