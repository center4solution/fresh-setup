import { NextFunction, Request, Response } from 'express';


class trimClass {

   public trim = async (
      req: Request,
      res: Response,
      next: NextFunction
   ) => {
      if (req.body) {
         await this.trimStringProperties(req.body);
      }
      if (req.params) {
         await this.trimStringProperties(req.params);
      }
      if (req.query) {
         await this.trimStringProperties(req.query);
      }
      next();
   }


   private trimStringProperties = async (
      obj: any
   ) => {
      let returnData: any = {};
      if (obj !== null && typeof obj === 'object') {
         for (const prop in obj) {
            // if the property is an object trim it too
            if (typeof obj[prop] === 'object') {
               returnData = this.trimStringProperties(obj[prop]);
            }

            // if it's a string remove begin and end whitespaces
            if (typeof obj[prop] === 'string') {
               returnData = obj[prop] = obj[prop].trim();
            }
         }
      }
      return returnData;
   };
}
const trimAll = new trimClass();
export default trimAll;
