import { Request, Response } from "express";

import {
   ReturnResponse,
} from "../../../helpers";

import {
   RES_MSG,
} from "../../../constant";

class AuthController {

   constructor() {

   }
   Login = async (
      req: Request,
      res: Response
   ) => {
      try {
         const {
            email,
            pass
         } = req.body
         
         const resMsg = {
            status: true,
            message: RES_MSG.LOGIN.SIGN_IN,
            data: {}
         };
         return ReturnResponse.success(res, resMsg);
      } catch (error: any) {
         console.error(`AuthController-SignUp catch error >>>`, error);

         let resMsg = {
            status: false,
            message: RES_MSG.ERROR,
         };
         return ReturnResponse.error(res, resMsg);
      }
   };
}
export default new AuthController();
