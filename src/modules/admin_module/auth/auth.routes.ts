import * as express from 'express';

import AdminAuthController from './auth.controller';

class AdminAuthRouters {
   public path = '';
   public router = express.Router();
   constructor() {
      this.intializeRoutes();
   }
   public intializeRoutes() {
      this.router.all(
         `${this.path}*`,
      ).post(
         `${this.path}/login`,
         AdminAuthController.Login
      );
   }
}
export let AdminAuthRouter = new AdminAuthRouters;

