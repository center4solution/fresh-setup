import { Request, Response } from "express";


// const admin = require('firebase-admin');

import {
   Redis_Helper,
   ReturnResponse,
} from "../../../helpers";

import {
   RES_MSG,
} from "../../../constant";

class PublicController {

   constructor() {

   }
   CheckRedis = async (
      req: Request,
      res: Response
   ) => {
      try {
         console.log(`CheckRedis >>> `);

         for(let i=1; i<=3; i++){
            for(let j=1; j<=5; j++){
               const expiresTime = Math.floor(Math.random() * (60 - 10 + 1)) + 1;
               const data = {
                  key: `setHashTable:folder${i}:key${j}`,
                  expiresTime: expiresTime
               };

               Redis_Helper.UpdateKeyData(
                  `setHashTable:folder${i}:key${j}`, // key
                  data,
                  expiresTime, /// expires
               );
            }
         }



         const resMsg = {
            status: true,
            message: RES_MSG.SUCCESS,
            data: {}
         };
         return ReturnResponse.success(res, resMsg);
      } catch (error: any) {
         console.error(`PublicController-SignUp catch error >>>`, error);

         let resMsg = {
            status: false,
            message: RES_MSG.ERROR,
         };
         return ReturnResponse.error(res, resMsg);
      }
   };
}
export default new PublicController();
