import * as express from 'express';

import PublicController from './public.controller';

class PublicRouters {
   public path = '';
   public router = express.Router();
   constructor() {
      this.intializeRoutes();
   }
   public intializeRoutes() {
      this.router.all(
         `${this.path}*`,
      ).post(
         `${this.path}/checkRedis`,
         PublicController.CheckRedis
      );
   }
}
export let PublicRouter = new PublicRouters;

