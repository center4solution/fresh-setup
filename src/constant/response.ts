import { CONFIG } from "../config";

export const RESPONSES = {
   SUCCESS: 200,
   CREATED: 201,
   ACCEPTED: 202,
   NOCONTENT: 204,
   BADREQUEST: 400,
   UNAUTHORIZED: 401,
   FORBIDDEN: 403,
   NOTFOUND: 404,
   TIMEOUT: 408,
   TOOMANYREQ: 429,
   INTERNALSERVER: 500,
   BADGATEWAYS: 502,
   SERVICEUNAVILABLE: 503,
   GATEWAYTIMEOUT: 504
};

export const MIDDLEWARE_RESPONSE = {
   JWTERROR: "Unauthorize Request",
   PERMISSION_DENIED: "Permission denied for this user",
   ONLY_LOGIN_WORKS: "The feature is temporarily disabled"
};

export const RES_MSG = {
   SUCCESS: "Your request is executed successfully",
   ERROR: "Oops! Something went wrong. Please try again",

   USERNAME: {
      DUPLICATE: "Duplicate user name",
   },

   REGISTOR: {
      DUPLICATE: "email already in use",
      INVALID_EMAIL: 'Invalid email'
   },

   LOGIN:{
      SIGN_IN:"Login successfully",
      INVALID_CREDENTIAL:"Invalid credentials provided",
   },

   eng: {
      SUCCESS: "Your request is executed successfully",
      ERROR: "Oops! Something went wrong. Please try again",

      CHANGE_PASSWORD: {
         RESETSUCCESS: "Password has been reset",
         SAME: "Old password or new Password is same ",
         PASSWORDNOTMATCHED: "Password and confirm password do not match",
         WRONGPASSWORD: "You entered a wrong password",
         VALID_PASS: "New password atleast one special character one number and one alphabet"
      },

      FORGOT_PASSWORD: {
         EMAILSENT: "An email with password reset instructions has been sent to the email address provided by you",
         EMAILNOTEXIST: "Sorry, we don't recognise that email address",
      },
   },


   hin: {
      SUCCESS: "आपका अनुरोध सफलतापूर्वक निष्पादित हो गया है",
   }

};
