import * as redis from 'redis';
import { createClient } from 'redis';

import { CONFIG } from '../config';


class RedisConn {
   public client: any = createClient();

   public redis_conn: any = CONFIG.REDIS_CONN;

   constructor() {
      this.client = redis.createClient({
         url: this.redis_conn
      });
      this.client.connect();

      this.client.on("error", () => {
         console.error('Redis not connected');
      });
      this.client.on("connect", () => {
         // console.error("Redis Connected");
      });
      this.client.on("close", () => {
         console.error('Redis close');
      })

   }

   setString = async (key: string, value: string, expires = 0, database = '') => {
      if (database !== '') {
         this.client.select(database);
      }
      return new Promise( async(resolve, reject) => {
         const clientSet = await this.client.set(key, value);
         if (clientSet && expires > 0) {
            this.client.expire(key, expires * 10); // 60
         }
         resolve(true);
      });
   }

   // Get String value for given key
   getString = async (key: string, database: any = '') => {
      if (database !== "") {
         this.client.select(database);
      }
      return await this.client.get(key);
   }

   destroyDb = async (dbKey: string) => {
      return new Promise((resolve) => {
         this.client.del(dbKey, (err: any, response: any) => {
            if (response === 1) {
               resolve(true);
            } else {
               resolve(false);
            }
         });
      });
   }

   setHashString = async (
      key: string,
      field: any,
      value: string,
      database = ''
   ) => {
      try {
         if (database !== '') {
            this.client.select(database);
         }
         return this.client.hSet(key, field, Buffer.from(value));
      } catch (error) {
         console.error('Hset', error);
         return null;
      }
   }

   setHashTable = async (
      key: any,
      field: any,
      value: any,
      expires: any = 0,
      database: any = ''
   ) => {
      if (database !== '') {
         this.client.select(database);
      }
      return new Promise((resolve, reject) => {
         this.client.hSet(key, field.toUpperCase(), Buffer.from(value), (err: any, reply: any) => {
            if (err) {
               return reject(err);
            }
            // Add Expire Time if provided
            if (expires !== 0) {
               this.client.expire(key, (expires * 60));
            }
            resolve(reply);
         });
      });
   }

   getHashTableData = async (
      key: any,
      field: any,
      database: any = '0'
   ) => {
      if (database !== '') {
         this.client.select(database);
      }
      return await this.client.hGet(key, field.toUpperCase());
   }

   getHashTableAllData = async (
      key: any,
      database: any = '0'
   ) => {
      if (database !== '') {
         this.client.select(database);
      }
      return await this.client.hVals(key);
   }

   deleteString = async (
      key: string
   ) => {
      this.client.del(key);
      return true;
   }
}
export let Redis = new RedisConn();
