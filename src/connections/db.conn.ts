import { Sequelize } from "sequelize";
import { DBInterface } from "../interface";
import { CONFIG } from "../config";


let USER: any = CONFIG.DB.USER;
let PASSWORD: any = CONFIG.DB.PASSWORD;
let DB_NAME: any = CONFIG.DB.DB_NAME;
let DB_HOST: any = CONFIG.DB.HOST;
let HOST_READ: any = CONFIG.DB.HOST_READ;

class DataBase implements DBInterface {
   public conn_write: Sequelize;
   public conn_read: Sequelize;

   // this.connectionWrite = `mysql://${config.DB_USER}:${config.DB_PASSWORD}@${config.DB_HOST_WRITE}/${config.DB_DATABASE}`;


   constructor() {      
      this.connRead()
      this.connWrite()
   }

   private connWrite = async() => {
      this.conn_write = new Sequelize(DB_NAME, USER, PASSWORD, {
         dialect: 'mysql',
         storage: 'database.mysql',
         port: 3306,
         replication: {
            read: [
               { host: DB_HOST, username: USER, password: PASSWORD }
            ],
            write: { host: DB_HOST, username: USER, password: PASSWORD }
         },
         logging: false,
         define: {
            // charset: 'utf8',
            // collate: 'utf8_general_ci',
            freezeTableName: true,
            underscored: true,
            timestamps: true,
            createdAt: "created_at",
            updatedAt: "updated_at"
         },
         pool: {
            max: 5,
            min: 0,
            idle: 20000,
            acquire: 60000
         }
      });

      this.conn_write.authenticate().then(() => {
         console.debug(`${DB_NAME} DB write Connection has been established successfully.`);
      }).catch((error: any) => {
         console.error(`${DB_NAME} DB write connection Unable to connect to the database: ${DB_NAME}`, error);
      });
   }
   
   private connRead = async() => {
      const connectionRead = `mysql://${USER}:${PASSWORD}@${HOST_READ}/${DB_NAME}`;

      this.conn_read = new Sequelize(connectionRead, {
         dialect: 'mysql',
         storage: 'database.mysql',
         port: 3306,
         // replication: {
         //    read: [
         //       { host: DB_HOST, username: USER, password: PASSWORD }
         //    ],
         //    write: { host: DB_HOST, username: USER, password: PASSWORD }
         // },
         logging: false,
         define: {
            // charset: 'utf8',
            // collate: 'utf8_general_ci',
            freezeTableName: true,
            underscored: true,
            timestamps: true,
            createdAt: "created_at",
            updatedAt: "updated_at"
         },
         pool: {
            max: 5,
            min: 0,
            idle: 20000,
            acquire: 60000
         }
      });

      this.conn_read.authenticate().then(() => {
         console.debug(`${DB_NAME} DB read Connection has been established successfully.`);
      }).catch((error: any) => {
         console.error(`${DB_NAME} DB read connection Unable to connect to the database: ${DB_NAME}`, error);
      });
   }

}
const db = new DataBase();
export const DATABASE = {
   write: db.conn_write,
   read: db.conn_read
};
